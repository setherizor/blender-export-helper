# S&Box Citizen's Blender Animation Exporter
# @Author Setherizor
# @LastUpdated <deploy-type>
# @LastUpdated <deploy-date>

# bpy.ops.export_helper.fbx()

# Warning: This export script will replace the exported files when ran. This could be descructive if you have colliding filenames.
# To understand what this script does, reference the following document and start reading from the bottom of the code here
# https://github.com/Ali3nSystems/Blender-Control-Rig-for-Terry/blob/main/Documentation/B.%20Poses%2C%20Animations%20and%20Shape%20Keys/c.%20Exporting%20Poses%20and%20Animations.pdf

# If you run into issues, you may need to clear out your "Unused Data Blocks" in the "File > Clean Up" Menu.
